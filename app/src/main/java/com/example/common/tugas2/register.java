package com.example.common.tugas2;

/**
 * Created by MDSO on 9/29/2016.
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class register extends Activity implements View.OnClickListener {
    Button back;
    Intent kembali;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        back = (Button) findViewById(R.id.btnCancel);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        kembali = new Intent(this, halaman1.class);
        startActivity(kembali);
    }
}