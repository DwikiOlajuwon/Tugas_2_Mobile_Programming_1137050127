package com.example.common.tugas2;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class halaman1 extends AppCompatActivity {
    Button btnlogin, btnregister;
    EditText edusername, edpassword;
    int counter = 3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halaman1);
        btnlogin = (Button) findViewById(R.id.btnlogin);
        btnregister = (Button) findViewById(R.id.btnregister);
        edusername = (EditText) findViewById(R.id.edusername);
        edpassword = (EditText) findViewById(R.id.edpassword);
        btnregister = (Button) findViewById(R.id.btnregister);

        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), register.class);
                startActivity(intent);
            }
        });

                btnlogin.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick (View v){
            if (edusername.getText().toString().equals("admin") && edpassword.getText().toString().equals("admin")) {
                Toast.makeText(getApplicationContext(), "LoginSukses", Toast.LENGTH_LONG);
                Intent loginsukses = new Intent(halaman1.this, halaman2.class);
                startActivity(loginsukses);
            } else {
                Toast.makeText(getApplicationContext(), "Username atau Password Salah", Toast.LENGTH_SHORT).show();
            }
        }
    });
    }

    public void browser1(View view){
        Intent browserIntent=new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.facebook.com"));
        startActivity(browserIntent);
    }

    public void browser2 (View view){
        Intent browserIntent=new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.gmail.com"));
        startActivity(browserIntent);
    }

    public void browser3 (View view){
        Intent browserIntent=new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.twitter.com"));
        startActivity(browserIntent);
    }
}
